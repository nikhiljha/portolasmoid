function getTime(callback) {
	request("http://epicteam.app:3020/api/time", function(data) {
		if(data.length === 0) return false;

		data = JSON.parse(data);
		var rate = data.text;
		callback(rate);

	});
	
	return true;
}

function request(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if(xhr.readyState === 4) {
			callback(xhr.responseText);
		}
	};
	xhr.open('GET', url, true);
	xhr.send('');
}
