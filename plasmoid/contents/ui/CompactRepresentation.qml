import QtQuick 2.0
import QtQuick.Layouts 1.1
import org.kde.plasma.components 2.0    as PlasmaComponents
import org.kde.plasma.core 2.0          as PlasmaCore
import "../code/network.js" as Network
 
Item
{
    id: root

    property string time_str: '...'    

    Layout.preferredWidth:  date_label.width  + 4   // 2 px of separation 
    Layout.maximumWidth:    date_label.width  + 4
    Layout.preferredHeight: date_label.height + 4
    Layout.maximumHeight:   date_label.height + 4

    PlasmaComponents.Label
    {
        id:                 date_label
        text:               root.time_str
        anchors.centerIn:   parent
    }

    Timer {
        id: refreshTimer
        interval: 60000
        running: true
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            var result = Network.getTime(function(r) {
                root.time_str = r;
            })
        }
    }
}
